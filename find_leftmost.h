
size_t fileavl_find_leftmost(
	struct fileavl* this,
	size_t tree_index,
	size_t starting_node);
