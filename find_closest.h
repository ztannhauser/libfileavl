
enum fileavl_find_closest_rounding 
{
	fafcr_exact,
	fafcr_round,
	fafcr_always_up,
	fafcr_always_down
};

size_t fileavl_find_closest(struct fileavl* this,
	int index,
	fileavl_find_comparator compare,
	void* b,
	enum fileavl_find_closest_rounding rounding);
