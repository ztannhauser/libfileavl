# file-avl

This repository holds functions for mantaining a binary search tree in a file. Nodes can be added with any size, and space allocated for nodes is reused when the nodes are deleted. There are functions (`file_avl_malloc`, `file_avl_realloc`, and `file_avl_free`) which allocate space in the file without adding data to the tree. This can be useful for nodes whose content contains many variable-length strings which one does not want to be in continuous memory.

