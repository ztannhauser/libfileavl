#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>

#include "comparator.h"
#include "struct.h"

#include "private/push.h"
#include "private/pop.h"
#include "private/zero_header.h"
#include "private/0th_tree_comparator.h"

void fileavl_remove(struct fileavl* this, unsigned tree_index, size_t loc)
{
	this->roots[tree_index] = fileavl_pop(
		this, this->roots[tree_index], loc);
	lseek(this->fd, loc, SEEK_SET);
	fileavl_zero_header(this->fd);
	this->roots[0] = fileavl_push(
		this, this->roots[0], loc, fileavl_0th_tree_comparator);
}
