
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>

#include "comparator.h"
#include "struct.h"

#include "private/get/left.h"

size_t fileavl_find_leftmost(
	struct fileavl* this,
	size_t tree_index,
	size_t starting_node)
{
	size_t before = 0, root = starting_node ?: this->roots[tree_index];
	while(root)
	{
		before = root;
		root = fileavl_get_node_left(this, root);
	}
	return before;
}
