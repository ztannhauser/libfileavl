
#include <assert.h>
#include <stdbool.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"

#include "comparator.h"
#include "struct.h"
#include "for_each_node_callback.h"
#include "find_leftmost.h"

#include "private/get/next.h"

void fileavl_for_each_node_cached(
	struct fileavl* this,
	size_t tree_index,
	fileavl_for_each_node_callback callback)
{
	ENTER;
	for(
		size_t
			here = fileavl_find_leftmost(this, tree_index, 0),
			next = fileavl_get_node_next(this, here)
		;
		here;
		here = next,
		next = fileavl_get_node_next(this, next))
	{
		verpv(here);
		verpv(next);
		callback(this->fd, here);
	}
	EXIT;
}
