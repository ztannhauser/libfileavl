#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"
#include "private/get/left.h"
#include "private/get/right.h"

#include "./struct.h"

void fileavl_for_each_filtered(struct fileavl* this, int tree_index,
	int (*compare)(struct fileavl* this, size_t loc, void* ptr),
	void* ptr, void (*callback)(size_t loc))
{
	ENTER;
	verpv(this);
	verpv(tree_index);
	verpv(compare);
	verpv(ptr);
	verpv(callback);
	size_t root = this->roots[tree_index];
	void go(size_t loc)
	{
		ENTER;
		verpv(loc);
		int c = compare(this, loc, ptr);
		verpv(c);
		if(c >= 0)
		{
			size_t left = fileavl_get_node_left(this, loc);
			verpv(left);
			if(left)
			{
				go(left);
			}
		}
		if(c == 0)
		{
			callback(loc);
		}
		if(c <= 0)
		{
			size_t right = fileavl_get_node_right(this, loc);
			verpv(right);
			if(right)
			{
				go(right);
			}
		}
		EXIT;
	}
	if(root)
	{
		go(root);
	}
	EXIT;
}
