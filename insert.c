
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"

#include "comparator.h"
#include "struct.h"
#include "node_header.h"
#include "malloc.h"
#include "find_comparator.h"
#include "find_closest.h"
#include "private/push.h"

size_t fileavl_insert(struct fileavl* this,
	int tree_index, void* data, size_t size)
{
	ENTER;
	verpv(data);
	verpv(size);
	size_t loc = fileavl_malloc(this, size);
	verpv(loc);
	lseek(this->fd, loc + sizeof(struct node_header), SEEK_SET);
	write(this->fd, data, size);
	this->roots[tree_index] = fileavl_push(this, this->roots[tree_index], loc,
		this->compares[tree_index]);
	EXIT;
	return loc;
}
