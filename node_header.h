struct node_header
{
	size_t parent;
	size_t height;
	size_t left, right;
	size_t prev, next;
	size_t size;
};
