#ifndef LIBFILEAVL_H
#define LIBFILEAVL_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>

struct fileavl;

typedef int (*fileavl_comparator)(struct fileavl* this, size_t a, size_t b);

struct node_header
{
	size_t parent;
	size_t height;
	size_t left, right;
	size_t prev, next;
	size_t size;
};

struct fileavl
{
	int fd;
	size_t n;
	size_t* roots;
	bool should_flock;
	bool should_flush;
	fileavl_comparator* compares;
};

int fileavl_open(struct fileavl* this,
	const char *pathname, int flags, mode_t mode,
	fileavl_comparator* compares, int n, bool should_lock);

size_t fileavl_insert(struct fileavl* this,
	int tree_index, void* data, size_t size);

void fileavl_close(struct fileavl* this);

size_t fileavl_malloc(struct fileavl* this, size_t size);

size_t fileavl_realloc(struct fileavl* this, size_t block, size_t size);

void fileavl_free(struct fileavl* this, size_t block);

typedef int (*fileavl_find_comparator)(struct fileavl* this, size_t a, void* b);

size_t fileavl_find(struct fileavl* this,
	int tree_index,
	fileavl_find_comparator compare,
	void* data);

void fileavl_for_each_filtered(struct fileavl* this, int tree_index,
	int (*filter)(struct fileavl* this, size_t loc, void* ptr),
	void* ptr, void (*callback)(size_t loc));

size_t fileavl_find_leftmost(
	struct fileavl* this,
	size_t tree_index,
	size_t starting_node);

typedef void (*fileavl_for_each_node_callback)(int fd, size_t node);

void fileavl_for_each_node_cached(
	struct fileavl* this,
	size_t tree_index,
	fileavl_for_each_node_callback callback);
	
void fileavl_remove(struct fileavl* this, unsigned tree_index, size_t loc);
#endif
