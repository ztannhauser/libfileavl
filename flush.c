
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>

#include "comparator.h"
#include "struct.h"

void fileavl_flush(struct fileavl* this)
{
	lseek(this->fd, 0, SEEK_SET);
	write(this->fd, this->roots, sizeof(size_t) * this->n);
	this->should_flush = false;
}
