
#include <assert.h>
#include <unistd.h>
#include <stddef.h>

#include "../node_header.h"

void fileavl_zero_header(int fd)
{
	struct node_header h = {0};
	write(fd, &h, offsetof(struct node_header, size));
}
