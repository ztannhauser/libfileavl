
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stddef.h>

#include "../node_header.h"
#include "../comparator.h"
#include "../struct.h"
#include "../find_leftmost.h"

#include "get/parent.h"
#include "get/right.h"
#include "get/left.h"
#include "get/next.h"
#include "get/prev.h"

#include "set/parent.h"
#include "set/right.h"
#include "set/left.h"
#include "set/prev.h"
#include "set/next.h"

#include "consider_rebalance.h"

size_t fileavl_pop(struct fileavl* this, size_t groot, size_t gnode) {
/*	printf("\\begin{%s}\n", __FUNCTION__);*/
	size_t a(size_t root, size_t node) {
/*	printf("\\begin{%s}\n", __FUNCTION__);*/
/*		printf("root == %lu\n", root);*/
/*		printf("node == %lu\n", node);*/
		size_t parent = fileavl_get_node_parent(this, node);
		size_t old_node = node;
		size_t node_left = fileavl_get_node_left(this, node);
		size_t node_right = fileavl_get_node_right(this, node);
/*		printf("node_left == %lu\n", node_left);*/
/*		printf("node_right == %lu\n", node_right);*/
		if(node_left && node_right) {
/*			size_t temp = fileavl_find_leftmost(this, 0, node_right);*/
			size_t temp = fileavl_get_node_next(this, node);
			fileavl_set_node_right(
				this, node, node_right = a(node_right, temp));
			fileavl_set_node_parent(this, node_left, temp);
			if(node_right) {
				fileavl_set_node_parent(this, node_right, temp);
			}
			{
				struct node_header nh;
				lseek(this->fd, node, SEEK_SET);
				read(this->fd, &nh, offsetof(struct node_header, size));
/*				printf("nh.prev == %lu\n", nh.prev);*/
				if(nh.prev)
				{
					fileavl_set_node_next(this, nh.prev, temp);
				}
/*				printf("nh.next == %lu\n", nh.next);*/
				if(nh.next)
				{
					fileavl_set_node_prev(this, nh.next, temp);
				}
				lseek(this->fd, temp, SEEK_SET);
				write(this->fd, &nh, offsetof(struct node_header, size));
			}
/*			assert(0);*/
			node = fileavl_consider_rebalance(this, temp, parent);
		} else {
			if(node_left)
			{
				size_t mynext = fileavl_get_node_next(this, node);
				fileavl_set_node_next(this, node_left, mynext);
				if(mynext)
				{
					fileavl_set_node_prev(this, mynext, node_left);
				}
				fileavl_set_node_parent(this, node_left, parent);
				node =
					fileavl_consider_rebalance(this, node_left, parent);
			}
			else if(node_right)
			{
				size_t myprev = fileavl_get_node_prev(this, node);
				fileavl_set_node_prev(this, node_right, myprev);
				if(myprev)
				{
					fileavl_set_node_next(this, myprev, node_right);
				}
				fileavl_set_node_parent(this, node_right, parent);
				node =
					fileavl_consider_rebalance(this, node_right, parent);
			}
			else
			{
				size_t myprev = fileavl_get_node_prev(this, node);
				size_t mynext = fileavl_get_node_next(this, node);
/*				printf("mynext == %lu\n", mynext);*/
/*				printf("myprev == %lu\n", myprev);*/
				if(mynext)
				{
					fileavl_set_node_prev(this, mynext, myprev);
				}
				if(myprev)
				{
					fileavl_set_node_next(this, myprev, mynext);
				}
				node = 0;
/*				assert(0);*/
			}
		}
		if(root == old_node) {
/*	printf("\\end{%s}\n", __FUNCTION__);*/
			return node;
		}
		while(parent) {
			if(fileavl_get_node_left(this, parent) == old_node) {
				fileavl_set_node_left(this, parent, node);
			} else {
				fileavl_set_node_right(this, parent, node);
			}
			size_t parent_parent =
				fileavl_get_node_parent(this, parent);
			size_t old_parent = parent;
			parent = fileavl_consider_rebalance(
				this, parent, parent_parent);
			if(old_parent == root) {
				break;
			}
			old_node = old_parent;
			node = parent;
			parent = parent_parent;
		}
/*	printf("\\end{%s}\n", __FUNCTION__);*/
		return parent;
	}
	this->should_flush = true;
	size_t newroot = a(groot, gnode);
/*	printf("\\end{%s}\n", __FUNCTION__);*/
	return newroot;
}

