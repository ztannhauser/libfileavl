
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>

#include "../comparator.h"
#include "../struct.h"

#include "get/balance.h"
#include "get/left.h"
#include "get/right.h"

#include "set/update_height.h"
#include "set/right.h"
#include "set/left.h"

#include "rotate_left_node.h"
#include "rotate_right_node.h"

size_t fileavl_consider_rebalance(struct fileavl* this,
											  size_t x, size_t parent) {
	fileavl_update_height(this, x);
	int balance = fileavl_get_node_balance(this, x);
	if(balance > 1) {
		int c = fileavl_get_node_balance(
			this, fileavl_get_node_left(this, x));
		if(c >= 0) {
			return fileavl_rotate_right_node(this, x, parent);
		} else {
			fileavl_set_node_left(
				this, x,
				fileavl_rotate_left_node(
					this, fileavl_get_node_left(this, x), x));
			return fileavl_rotate_right_node(this, x, parent);
		}
	} else if(balance < -1) {
		int c = fileavl_get_node_balance(
			this, fileavl_get_node_right(this, x));
		if(c <= 0) {
			return fileavl_rotate_left_node(this, x, parent);
		} else {
			fileavl_set_node_right(
				this, x,
				fileavl_rotate_right_node(
					this, fileavl_get_node_right(this, x), x));
			return fileavl_rotate_left_node(this, x, parent);
		}
	}
	return x;
}

