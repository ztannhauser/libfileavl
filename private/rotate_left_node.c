
#include <unistd.h>
#include <stdbool.h>
#include <assert.h>

#include "../comparator.h"
#include "../struct.h"

#include "get/left.h"
#include "get/right.h"

#include "set/update_height.h"
#include "set/parent.h"
#include "set/left.h"
#include "set/right.h"

size_t fileavl_rotate_left_node(struct fileavl* this, size_t x,
											size_t parent) {
	size_t y = fileavl_get_node_right(this, x);
	size_t t = fileavl_get_node_left(this, y);
	fileavl_set_node_left(this, y, x);
	fileavl_set_node_right(this, x, t);
	fileavl_set_node_parent(this, y, parent);
	fileavl_set_node_parent(this, x, y);
	if(t)
	{
		fileavl_set_node_parent(this, t, x);
	}
	fileavl_update_height(this, x);
	fileavl_update_height(this, y);
	return y;
}

