
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "../comparator.h"
#include "../struct.h"

#include "get/size.h"

int fileavl_0th_tree_comparator(struct fileavl* this, size_t a,
											size_t b) {
	return fileavl_get_node_size(this, a) - fileavl_get_node_size(this, b);
}

