
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../node_header.h"
#include "../../comparator.h"
#include "../../struct.h"

void fileavl_set_node_left(struct fileavl* this, size_t node,
									   size_t new_left) {
	lseek(this->fd, node + offsetof(struct node_header, left), SEEK_SET);
	write(this->fd, &new_left, sizeof(size_t));
}

