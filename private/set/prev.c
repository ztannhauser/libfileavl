
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../node_header.h"
#include "../../comparator.h"
#include "../../struct.h"

void fileavl_set_node_prev(struct fileavl* this, size_t node,
									   size_t new_prev) {
	lseek(this->fd, node + offsetof(struct node_header, prev), SEEK_SET);
	write(this->fd, &new_prev, sizeof(size_t));
}

