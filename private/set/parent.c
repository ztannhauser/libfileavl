
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../struct.h"
#include "../../node_header.h"

void fileavl_set_node_parent(struct fileavl* this, size_t node,
										 size_t parent) {
	lseek(this->fd, node + offsetof(struct node_header, parent), SEEK_SET);
	write(this->fd, &parent, sizeof(size_t));
}
