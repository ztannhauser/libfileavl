
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../node_header.h"
#include "../../comparator.h"
#include "../../struct.h"

void fileavl_set_node_right(struct fileavl* this, size_t node,
										size_t new_right) {
	lseek(this->fd, node + offsetof(struct node_header, right), SEEK_SET);
	write(this->fd, &new_right, sizeof(size_t));
}
