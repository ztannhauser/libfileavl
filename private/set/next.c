
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../node_header.h"
#include "../../comparator.h"
#include "../../struct.h"

void fileavl_set_node_next(struct fileavl* this, size_t node,
									   size_t new_next) {
	lseek(this->fd, node + offsetof(struct node_header, next), SEEK_SET);
	write(this->fd, &new_next, sizeof(size_t));
}

