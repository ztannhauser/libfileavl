
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../struct.h"
#include "../../node_header.h"

void fileavl_set_node_height(struct fileavl* this, size_t node, size_t height)
{
	lseek(this->fd, node + offsetof(struct node_header, height), SEEK_SET);
	write(this->fd, &height, sizeof(size_t));
}
