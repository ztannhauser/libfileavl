
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>

#include "../../comparator.h"
#include "../../struct.h"

#include "height.h"

#include "../get/height.h"
#include "../get/left.h"
#include "../get/right.h"

#define max(a, b) (a > b ? a : b)

void fileavl_update_height(struct fileavl* this, size_t node)
{
	size_t
	lh = fileavl_get_node_height(this,
		fileavl_get_node_left(this, node)),
	rh = fileavl_get_node_height(this,
		fileavl_get_node_right(this, node));
	fileavl_set_node_height(this, node, max(lh, rh) + 1);
}

