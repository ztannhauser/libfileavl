
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"

#include "../node_header.h"

#include "../comparator.h"
#include "../struct.h"

#include "consider_rebalance.h"

#include "get/left.h"
#include "get/right.h"
#include "get/next.h"
#include "get/prev.h"
#include "set/height.h"
#include "set/left.h"
#include "set/parent.h"
#include "set/right.h"
#include "set/next.h"
#include "set/prev.h"

size_t fileavl_push(struct fileavl* this, size_t root, size_t loc,
					 fileavl_comparator compare) {
	ENTER;
	verpv(root);
	verpv(loc);
	size_t a(size_t node, size_t parent, size_t prev, size_t next) {
		if(!node) {
			fileavl_set_node_height(this, loc, 1);
			if(parent) fileavl_set_node_parent(this, loc, parent);
			if(next)
			{
				fileavl_set_node_next(this, loc, next);
				fileavl_set_node_prev(this, next, loc);
			}
			if(prev)
			{
				fileavl_set_node_prev(this, loc, prev);
				fileavl_set_node_next(this, prev, loc);
			}
			return loc;
		}
		int c = compare(this, node, loc);
		verpv(c);
		if(c > 0) {
			size_t left = fileavl_get_node_left(this, node);
			size_t newleft = a(left, node, prev, node);
			if(left != newleft)
			{
				fileavl_set_node_left(this, node, newleft);
			}
		} else {
			size_t right = fileavl_get_node_right(this, node);
			size_t newright = a(right, node, node, next);
			if(right != newright)
			{
				fileavl_set_node_right(this, node, newright);
			}
		}
		node = fileavl_consider_rebalance(this, node, parent);
		return node;
	}
	root = a(root, 0, 0, 0);
	this->should_flush = true;
	EXIT;
	return root;
}
