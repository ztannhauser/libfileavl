
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../node_header.h"
#include "../../struct.h"

size_t fileavl_get_node_prev(struct fileavl* this,
										 size_t node) {
	size_t prev;
	lseek(this->fd, node + offsetof(struct node_header, prev), SEEK_SET);
	read(this->fd, &prev, sizeof(size_t));
	return prev;
}

