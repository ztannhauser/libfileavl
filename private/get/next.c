
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../node_header.h"
#include "../../struct.h"

size_t fileavl_get_node_next(struct fileavl* this,
										 size_t node) {
	size_t next;
	if(node)
	{
		lseek(this->fd, node + offsetof(struct node_header, next), SEEK_SET);
		read(this->fd, &next, sizeof(size_t));
	}
	else
	{
		next = 0;
	}
	return next;
}

