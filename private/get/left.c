
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../node_header.h"
#include "../../struct.h"

size_t fileavl_get_node_left(struct fileavl* this,
										 size_t node) {
	size_t left;
	lseek(this->fd, node + offsetof(struct node_header, left), SEEK_SET);
	read(this->fd, &left, sizeof(size_t));
	return left;
}

