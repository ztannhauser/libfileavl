
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../node_header.h"
#include "../../struct.h"

size_t fileavl_get_node_parent(struct fileavl* this,
										 size_t node) {
	size_t parent;
	lseek(this->fd, node + offsetof(struct node_header, parent), SEEK_SET);
	read(this->fd, &parent, sizeof(size_t));
	return parent;
}

