
#include <unistd.h>
#include <stdbool.h>

#include "../../comparator.h"
#include "../../struct.h"

#include "height.h"
#include "left.h"
#include "right.h"

int fileavl_get_node_balance(struct fileavl* this, size_t node)
{
	if(node)
	{
		return
			fileavl_get_node_height(this,
				fileavl_get_node_left(this, node)
			)
			-
			fileavl_get_node_height(this,
				fileavl_get_node_right(this, node)
			);
	}
	else
	{
		return 0;
	}
}
