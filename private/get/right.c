
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../node_header.h"
#include "../../struct.h"

size_t fileavl_get_node_right(struct fileavl* this,
										  size_t node) {
	size_t right;
	lseek(this->fd, node + offsetof(struct node_header, right), SEEK_SET);
	read(this->fd, &right, sizeof(size_t));
	return right;
}

