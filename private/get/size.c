
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../comparator.h"
#include "../../node_header.h"
#include "../../struct.h"

size_t fileavl_get_node_size(struct fileavl* this,
										 size_t node) {
	size_t size;
	lseek(this->fd, node + offsetof(struct node_header, size), SEEK_SET);
	read(this->fd, &size, sizeof(size_t));
	return size;
}

