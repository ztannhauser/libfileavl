
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include "../../node_header.h"
#include "../../comparator.h"
#include "../../struct.h"

size_t fileavl_get_node_height(struct fileavl* this,
										   size_t node) {
	if(node) {
		size_t height;
		lseek(this->fd, node + offsetof(struct node_header, height), SEEK_SET);
		read(this->fd, &height, sizeof(size_t));
		return height;
	} else {
		return 0;
	}
}
