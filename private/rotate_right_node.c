
#include <unistd.h>
#include <stdbool.h>
#include <assert.h>

#include "../comparator.h"
#include "../struct.h"

#include "get/left.h"
#include "get/right.h"

#include "set/update_height.h"
#include "set/parent.h"
#include "set/left.h"
#include "set/right.h"

size_t fileavl_rotate_right_node(struct fileavl* this,
											 size_t y, size_t parent) {
	size_t x = fileavl_get_node_left(this, y);
	size_t t = fileavl_get_node_right(this, x);
	fileavl_set_node_right(this, x, y);
	fileavl_set_node_left(this, y, t);
	fileavl_set_node_parent(this, x, parent);
	fileavl_set_node_parent(this, y, x);
	if(t)
	{
		fileavl_set_node_parent(this, t, y);
	}
	fileavl_update_height(this, y);
	fileavl_update_height(this, x);
	return x;
}

