
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>

#include "../comparator.h"
#include "../struct.h"

#include "get/size.h"

int fileavl_0th_tree_find_closest_comparator(struct fileavl* this,
	size_t a, size_t* b)
{
	return fileavl_get_node_size(this, a) - *b;
}
