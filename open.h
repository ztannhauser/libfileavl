
int fileavl_open(struct fileavl* this,
	const char *pathname, int flags, mode_t mode,
	fileavl_comparator* compares, int n, bool should_lock);
