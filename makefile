MAKEFLAGS += --no-print-directory

.PHONY: default
.PHONY: installdeps

default: installdeps main.a

include arch.mk
include install.mk
include .dir.mk

deps += libdebug

deps_a = $(foreach dep,$(deps),../$(dep).a)

.%.c.o: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@
.%.cpp.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@
.dir.o:
	$(LD) -r $^ -o $@
%/.dir.o:
	$(LD) -r $^ -o $@

main: main.a

main.a: $(deps_a) .dir.o
	$(AR) -rsc main.a $(deps_a) .dir.o

downloaddeps: cmd1 = git clone git@gitlab.com:ztannhauser/$(dep).git
downloaddeps: cmd2 = if [ ! -d $(dep) ]; then $(cmd1); fi
downloaddeps: cmd3 = make -C $(dep) downloaddeps
downloaddeps:
	cd ../; $(foreach dep,$(deps),$(cmd2) && $(cmd3) &&) true

installdeps: cmd = $(MAKE) -C ../$(dep) install;
installdeps:
	$(foreach dep,$(deps),$(cmd))

clean:
	find . -type f -name '*.o' | xargs rm -vf main
