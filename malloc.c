
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "comparator.h"
#include "struct.h"
#include "find_comparator.h"
#include "find_closest.h"

#include "private/pop.h"
#include "private/zero_header.h"
#include "private/0th_tree_find_closest_comparator.h"

size_t fileavl_malloc(struct fileavl* this, size_t size)
{
	size_t loc =
		fileavl_find_closest(this, 0,
			 fileavl_0th_tree_find_closest_comparator,
						 &size, fafcr_always_up);
	if(loc) {
		this->roots[0] = fileavl_pop(
			this, this->roots[0], loc);
		lseek(this->fd, loc, SEEK_SET);
		fileavl_zero_header(this->fd);
	} else {
		this->should_flush = true;
		loc = lseek(this->fd, 0, SEEK_END);
		fileavl_zero_header(this->fd);
		write(this->fd, &size, sizeof(size_t));
	}
	return loc;
}
