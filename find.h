
size_t fileavl_find(struct fileavl* this,
	int tree_index,
	fileavl_find_comparator compare,
	void* data);
