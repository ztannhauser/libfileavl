
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "comparator.h"
#include "struct.h"
#include "find_comparator.h"

#include "private/get/right.h"
#include "private/get/left.h"

size_t fileavl_find(struct fileavl* this,
	int tree_index,
	fileavl_find_comparator compare,
	void* data)
{
	size_t root = this->roots[tree_index];
	if(!root) {
		return 0;
	}
	while(root) {
		int c = compare(this, root, data);
		if(c > 0) {
			root = fileavl_get_node_left(this, root);
		} else if(c < 0) {
			root = fileavl_get_node_right(this, root);
		} else {
			return root;
		}
	}
	return 0;
}
