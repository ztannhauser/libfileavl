
void fileavl_for_each_filtered(struct fileavl* this, int tree_index,
	int (*filter)(struct fileavl* this, size_t loc, void* ptr),
	void* ptr, void (*callback)(size_t loc));
