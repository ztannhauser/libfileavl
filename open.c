
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/file.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comparator.h"
#include "struct.h"

int fileavl_open(struct fileavl* this,
	const char *pathname, int flags, mode_t mode,
	fileavl_comparator* compares, int n, bool should_flock)
{
	int ret = 0;
	int fd = open(pathname, flags & ~O_CREAT, mode);
	this->should_flush = false;
	bool should_write_roots = flags & O_TRUNC;
	if(fd < 0)
	{
		if(flags & O_CREAT)
		{
			fd = open(pathname, flags, mode);
			if(fd < 0)
			{
				ret = -1;
			}
			else
			{
				ret = 1;
				should_write_roots = true;
			}
		}
		else
		{
			ret = -1;
		}
	}
	if(ret >= 0)
	{
		this->n = n;
		this->fd = fd;
		this->compares = compares;
		this->should_flock = should_flock;
		unsigned int roots_size = sizeof(size_t) * n;
		this->roots = malloc(roots_size);
		if(should_flock)
		{
			flock(fd, LOCK_EX);
		}
		if(should_write_roots)
		{
			bzero(this->roots, roots_size);
			write(fd, this->roots, roots_size);
			this->should_flush = true;
		}
		else
		{
			read(fd, this->roots, roots_size);
		}
	}
	return ret;
}










