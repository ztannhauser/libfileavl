
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>

#include "comparator.h"
#include "struct.h"
#include "private/0th_tree_comparator.h"
#include "private/push.h"
#include "private/zero_header.h"

void fileavl_free(struct fileavl* this, size_t block)
{
	if(block)
	{
		lseek(this->fd, block, SEEK_SET);
		fileavl_zero_header(this->fd);
		this->roots[0] =
			fileavl_push(this,
			this->roots[0], block,
							 fileavl_0th_tree_comparator);
	}
}

