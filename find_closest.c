

#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "comparator.h"
#include "struct.h"
#include "find_comparator.h"
#include "find_closest.h"

#include "private/get/right.h"
#include "private/get/left.h"

size_t fileavl_find_closest(struct fileavl* this,
	int tree_index,
	fileavl_find_comparator compare,
	void* data,
	enum fileavl_find_closest_rounding rounding)
{
	size_t root = this->roots[tree_index];
	if(!root) {
		return 0;
	}
	size_t left_bound = root, right_bound = root;
	int left_compar, right_compar;
	while(root) {
		int c = compare(this, root, data);
		if(c > 0) {
			right_compar = c;
			root = fileavl_get_node_left(this, root);
			left_bound = root;
		} else if(c < 0) {
			left_compar = c;
			root = fileavl_get_node_right(this, root);
			right_bound = root;
		} else {
			return root;
		}
	}
	size_t ret;
	switch(rounding) {
		case fafcr_exact: {
			ret = 0;
			break;
		}
		case fafcr_always_up: {
			ret = right_bound;
			break;
		}
		case fafcr_always_down: {
			ret = left_bound;
			break;
		}
		case fafcr_round: {
			ret =
				abs(left_compar) <
					abs(right_compar) ? left_bound : right_bound;
			break;
		}
	}
	return ret;
}
