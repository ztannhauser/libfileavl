#include <stdbool.h>

#include "./comparator.h"

struct fileavl
{
	int fd;
	size_t n;
	size_t* roots;
	bool should_flock;
	bool should_flush;
	fileavl_comparator* compares;
};
