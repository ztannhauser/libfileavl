
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

#include "node_header.h"
#include "comparator.h"
#include "struct.h"
#include "malloc.h"
#include "free.h"

#include "private/get/size.h"

size_t fileavl_realloc(struct fileavl* this, size_t block, size_t size)
{
	size_t ret;
	if(size)
	{
		if(block)
		{
			size_t asize = fileavl_get_node_size(this, block);
/*			printf("size == %lu\n", size);*/
/*			printf("asize == %lu\n", asize);*/
			if(size > asize)
			{
				ret = fileavl_malloc(this, size);
				char temp[asize];
				lseek(
					this->fd,
					block + sizeof(struct node_header),
					SEEK_SET);
				read(this->fd, temp, asize);
				lseek(
					this->fd,
					ret + sizeof(struct node_header),
					SEEK_SET);
				write(this->fd, temp, asize);
				fileavl_free(this, block);
			}
			else
			{
				ret = block;
			}
		}
		else
		{
			ret = fileavl_malloc(this, size);
		}
	}
	else
	{
		fileavl_free(this, block);
		ret = 0;
	}
	return ret;
}

