
#include <stdbool.h>
#include <unistd.h>
#include <sys/file.h>
#include <stdbool.h>
#include <stdlib.h>

#include "comparator.h"
#include "struct.h"
#include "flush.h"

void fileavl_close(struct fileavl* this)
{
	if(this->should_flush)
	{
		fileavl_flush(this);
	}
	if(this->should_flock)
	{
		flock(this->fd, LOCK_UN);
	}
	free(this->roots);
	close(this->fd);
	this->fd = -1;
}
