
#include <assert.h>
#include <stdbool.h>
#include <unistd.h>

#include "comparator.h"
#include "struct.h"
#include "for_each_node_callback.h"
#include "find_leftmost.h"

#include "private/get/next.h"

void fileavl_for_each_node(
	struct fileavl* this,
	size_t tree_index,
	fileavl_for_each_node_callback callback)
{
	for(size_t here = fileavl_find_leftmost(this, tree_index, 0);
		here;
		here = fileavl_get_node_next(this, here))
	{
		callback(this->fd, here);
	}
}
