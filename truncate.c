
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <strings.h>

#include "private/zero_header.h"

#include "node_header.h"
#include "comparator.h"
#include "struct.h"

void fileavl_truncate(struct fileavl* this)
{
	size_t size = this->n * sizeof(size_t);
	ftruncate(this->fd, size);
	bzero(this->roots, size);
	this->should_flush = true;
}

